<div class="container">
	<h1 id="logo">Penzias and Wilson</h1>

	<p class="lead">
		From the hands that brought you <a href="http://eponymous4.com/">Eponymous 4</a> and <a href="http://emptyensemble.com/">Empty Ensemble</a>.
	</p>

	<ul class="list-inline">
		<li><a href="http://twitter.com/PenziasWilson"><img src="<?php echo OBSERVANTRECORDS_CDN_BASE_URI ?>/web/images/icons/twitter.png" /></a></li>
	</ul>

</div>

<img src="/sites/all/themes/penziasandwilson/dark_balcony-half.jpg" class="bg" />